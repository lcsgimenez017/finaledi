# finaledi

//****Pasos iniciales ****//

1-Clone el repositorio enviado por el profesor y sobre la carpeta descargada "docker_final" ejecute el comando sudo docker-compose up. 

2-Utilice la imagen de portainer descargadada de otro repositorio y en la vista de containers le dí start a los contenedores "docker_final_php-apache_1" y a "docker_final_db_1" .

3.Me dirigí a la consolsa para conectarme a Mysql


//****Pasos realizados para crear la base de datos:****//

1. Ejecuté sobre consola mysql -u root -p 
Enter password : ****

2. CREATE DATABASE finaledi;

3. Visualice los campos que componían los archivos.

//****Creacion de tablas para archivos disponibilizados****//


1.USE finaledi ;

2. CREATE TABLE `finaledi`.`provincias` ( `Id_Provincia` INT NOT NULL , `Centroide_lat` VARCHAR(255) NOT NULL , `Centroide_lon` VARCHAR(255) NOT NULL , `Nombre` VARCHAR(100) NOT NULL , `Nombre_Completo` VARCHAR(100) NOT NULL , `Id_Categoria` INT NULL , `Id_Fuente` INT NULL ) ;

3. CREATE TABLE `finaledi`.`provincias` ( `Id_Provincia` INT NOT NULL , `Centroide_lat` VARCHAR(255) NOT NULL , `Centroide_lon` VARCHAR(255) NOT NULL , `Nombre` VARCHAR(100) NOT NULL , `Nombre_Completo` VARCHAR(100) NOT NULL , `Id_Categoria` INT NULL , `Id_Fuente` INT NULL ) ;

4. CREATE TABLE `finaledi`.`categoria` ( `Id_Categoria` INT NOT NULL , `Categoria` VARCHAR(100) NOT NULL ) ;

5. CREATE TABLE `finaledi`.`fuente` ( `Id_Fuente` INT NOT NULL , `Fuente` VARCHAR(100) NOT NULL ) ;

6. CREATE TABLE `finaledi`.`calles` ( `Id_Calle` INT NOT NULL , `Nombre` VARCHAR(100) NULL , `Altura_Fin_Derecha` INT NOT NULL , `Altura_Fin_Izquierda` INT NOT NULL , `Altura_Inicio_Derecha` INT NOT NULL , `Altura_Inicio_Izquierda` INT NOT NULL , `Id_Categoria` INT NOT NULL , `Id_Departamento` INT NOT NULL , `Id_Fuente` INT NOT NULL , `Id_Provincia` INT NOT NULL ) ;

7. CREATE TABLE `finaledi`.`departamentos` ( `Id_Departamento` INT NOT NULL , `Centroide_lat` VARCHAR(255) NOT NULL , `Centroide_lon` VARCHAR(255) NOT NULL , `Nombre` VARCHAR(100) NOT NULL , `Nombre_Completo` VARCHAR(100) NOT NULL , `Id_Fuente` INT NOT NULL , `Id_Provincia` INT NOT NULL , `Id_Categoria` INT NOT NULL ) ;

8. CREATE TABLE `finaledi`.`municipios` ( `Id_Municio` INT NOT NULL , `Centroide_lat` VARCHAR(255) NOT NULL , `Centroide_lon` VARCHAR(255) NOT NULL , `Nombre` VARCHAR(100) NOT NULL , `Nombre_Completo` VARCHAR(100) NOT NULL , `Id_Fuente` INT NOT NULL , `Id_Provincia` INT NOT NULL , `Id_Categoria` INT NOT NULL ) ;


//****Exportación de la base de datos****//

1.Desde consola ejecuté : cd /var/lib/mysql

2.Comando : mysqldump -u root -p finaledi > backup.sql

//****Descarga de proyecto y carga de db****//

1. git clone https://gitlab.com/lcsgimenez017/finaledi.git ; 

2. Localmente dentro de Mysql crear la base de datos finaledi (CREATE DATABASE finaledi;) .

3. Fuera del motor de base de datos ejecutar mysql –u usuario_mysql -p nombre_bbdd < backup.sql

